class Samochod: # PEP8 - uzycie self
	def __init__(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
	

s1 = Samochod('czewony', 'fiat')
s2 = Samochod('czarny', 'bmw')

def run(sam):
	print("Wszystko gotowe, jedziemy...", sam.kolor, sam.marka)

run(s1)
run(s2)
	