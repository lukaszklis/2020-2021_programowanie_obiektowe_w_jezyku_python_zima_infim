class Samochod: # PEP8 - uzycie self
	def __init__(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
		self.paliwo = 0
	
	def zatankuj(self, ile):
		self.paliwo += ile
	
	def run(self):
		if self.paliwo < 15:
			print("Za malo paliwa... zatankuj!")
		else:
			print("Wszystko gotowe, jedziemy...",
				  self.kolor, 
				  self.marka,
				  self.paliwo)
			self.paliwo -= 15

s1 = Samochod('czewony', 'fiat')
s2 = Samochod('czarny', 'bmw')

s1.zatankuj(30)

s1.run()
s2.run()

s1.run()

s1.run()