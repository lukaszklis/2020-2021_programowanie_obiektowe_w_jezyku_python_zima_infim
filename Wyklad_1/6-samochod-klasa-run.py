class Samochod: # PEP8 - uzycie self
	def __init__(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
	
	def run(self):
		print("Wszystko gotowe, jedziemy...",
			  self.kolor, 
			  self.marka)

s1 = Samochod('czewony', 'fiat')
s2 = Samochod('czarny', 'bmw')

s1.run()
s2.run()
	