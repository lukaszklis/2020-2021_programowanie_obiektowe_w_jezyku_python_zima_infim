class Samochod: # PEP8 - uzycie self
	def __init__(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
		self.__paliwo = 0 # PEP8 - podloga na poczatku nazwy oznacza pole prywatne
	
	def zatankuj(self, ile):
		self.__paliwo += ile
	
	def run(self):
		if self.__paliwo < 15:
			print("Za malo paliwa... zatankuj!")
		else:
			print("Wszystko gotowe, jedziemy...",
				  self.kolor, 
				  self.marka,
				  self.__paliwo)
			self.__paliwo -= 15

s1 = Samochod('czewony', 'fiat')
s2 = Samochod('czarny', 'bmw')

s1.zatankuj(30)

# nowe pole, inna nazwa, niz ta w klasie
s1.__paliwo = 100
print("Stan paliwa w s1:", s1.__paliwo)
# jak sprawdzic nazwe w klasie, czy mozna takie pole w klasie zmienic


s1.run()
s2.run()

s1.run()

s1.run()